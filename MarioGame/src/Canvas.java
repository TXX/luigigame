import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Canvas extends JPanel{

	public Canvas() {
		
	
	}
	
	public void paint(Graphics g) {
		if(g instanceof Graphics2D) {
			Graphics2D g2D = (Graphics2D) g;
			g2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		}
		
		
		g.setFont(new Font(g.getFont().getFontName(), g.getFont().getStyle(), 200 ));
		//g.drawString("æøå ÆØÅ", 0, 200);
		
		g.drawLine(0, 400, 600, 400);
		g.drawRect(200, 360, 25, 40);
	}

}
